Perf Evaluator
================================================================================

This script aims to automate performance evaluation of programs under specified configurations. Following some rules the user specifies a .json file with the test suits that will be run. The configuration file expecifies environment variable, input files, parameters, etc. 


Requirements
--------------------------------------------------------------------------------

To install the requirements you need to first install python 3.8.0 and python3-venv. Then run this commands inside this project root: 

```bash
# Creates a new python environment named *name*:
python3 -m venv *name*

# Activate the environment (needed every time you use the script):
source *name*/bin/activate

# Install the requirements:
pip install -r requirements.txt
```

Basic Run
--------------------------------------------------------------------------------

Before running the script, you need to have the environment activated.

```bash
# Run the script (you can omit python3 if you set up chmod first).
# Without any parameter, the script will search for a file named config.json and output to result.json:
python3 ./main.py
# You can add -h for more help with usage.
```

Configuration File WIP
--------------------------------------------------------------------------------
Below is the list of parameters you can tune in the json file:

- **name** - the base name of the executable that the script will run, this will later be appended to form the full name;
- **exec-folder** - folder where the executables are;
- **cwd** - folder where the executables will be run;
- **input-folder** - folder where the input files are;
- **input-files** - list of file names in the input folder;
- **repeat** - number of times that each configuration will be repeated to calculate the average;
- **output-folder** - folder where the output files of each configuration will be dumped;
- **result-file** - full name of the file where the time output will be written;
- **variations** - object that specifies how the executables names are generated:
**Each executable name is formed by name+sep+types**
    - **sep** - optional separator that separates the base name from the type when the executables names are generated;
    - **types** - list of executables types;
- **configs** WIP - list of object that may specify configurations to some executables types:
    - **env** - list of objects containing environment variables to try:
        - **name** - name of the environment variable;
        - **range** or **list** - ranges are triplets of numbers that works the same way as python ranges do, whereas list are string of value to use;
    - **param** WIP - list of objects containing executable parameters to try;
    - **types** - list of types that the current configuration should be applied on;
- **graph** WIP - object that specifies how to automatically generate the graphs using matplotlib;

