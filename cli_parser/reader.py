import argparse


def read():
    parser = argparse.ArgumentParser(description='Programs profile and performance graphs.')
    parser.add_argument('-filename', '-f', required=False, default='./config.json',
                        help='The name of the configuration file.')
    parser.add_argument('-result', '-r', required=False, default='./result.json',
                        help='The name of the result file.')
    parser.add_argument('-mode', '-m', required=False, default='perf', choices=['perf', 'graph'],
                        help='The running mode.')
    return parser.parse_args()
