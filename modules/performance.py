import json
from collections import defaultdict

import profiler
import config_parser


def save_resut(result, result_file):
    with open(result_file, 'w') as r_file:
        json.dump(result, r_file)


def eval_performance(filename):
    parser = config_parser.Parser(filename)
    repeat = parser['repeat']
    cwd = parser['cwd']

    ndict = lambda: defaultdict(ndict)  # Nested defaultdicts
    result = ndict()
    # For each executable type (t):
    for t, exec_name, exec_commands in parser.commands():
        # Get the configs for this type:
        environments, parameters = parser.specific_config(t)
        # TODO Iterate over each specific param too:
        # For each environment configuration:
        for id, config in enumerate(environments):
            env_str = ', '.join(map(str, config.items()))
            if env_str: print(' \n - Environment -', env_str)

            config_name = 'default' if id == 0 else 'config-%d' % (id + 1)
            # For each command:
            for cmd, input_file, out_file in exec_commands:
                times = profiler.perf(cmd, out_file, cwd, repeat, config)
                result[exec_name][config_name]['files'][input_file] = times
                result[exec_name][config_name]['env'] = env_str

    save_resut(result, parser['result-file'])
