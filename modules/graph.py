import os
import itertools

import numpy as np
import matplotlib.pyplot as plt

import config_parser


def gen_graph(type, file, result, output_folder):
    configs = result[type]
    x, y = [], []
    runs = 0
    for id, config in configs.items():
        x.append(id)
        runs = len(config['files'][file])
        y.append(np.mean(config['files'][file]))

    plt.plot(x, y)
    plt.title('Runnig time of %s with %s as input' % (type, file))
    plt.xlabel('Configuration')
    plt.ylabel('Mean time (%d runs)' % runs)
    plt.savefig(os.path.join(output_folder, '%s_%s.png' % (type, file)))
    plt.clf()


def gen_graphs(config_filename, result_filename):
    # TODO Rename the package:
    cparser = config_parser.Parser(config_filename)
    output_folder = cparser["output-folder"]
    fparser = cparser['graphs']
    result = config_parser.Parser(result_filename).data
    for graph in itertools.product(fparser['types'], fparser['files']):
        gen_graph(*graph, result, output_folder)
