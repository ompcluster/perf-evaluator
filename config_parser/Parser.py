import json
import yaml
import pathlib
from os import path
from itertools import cycle, product


def parse_variable(config):
    name_cycle = cycle([config['name']])
    values = map(str, config.get('values') or range(*config['range']))
    return zip(name_cycle, values)


def parse_env(env):
    yield from map(dict, product(*map(parse_variable, env)))


def parse_param(param):
    pass


class Parser:
    def __init__(self, filename):
        with open(filename) as file:
            suffix = pathlib.Path(filename).suffix
            if suffix == '.json':
                self.data = json.load(file)
            elif suffix == '.yaml':
                self.data = yaml.safe_load(file)
            else:
                raise ValueError('Unsupported extension %s' % suffix)
            self.data['configs'] = self.data.get('configs', [])  # Default value

    def __getitem__(self, key):
        return self.data[key]

    def execs_fullname(self):
        name = self.data['name']
        base = path.join(self.data['exec-folder'], name)
        sep = self.data['variations']['sep']
        return [(t, name + sep + t, base + sep + t) for t in
                self.data['variations']['types']]

    def input_files(self):
        return [(inf, path.join(self.data['input-folder'], inf)) for inf in
                self.data['input-files']]

    def exec_cmds(self, input_files, exec_name, exec_fullname):
        input_param = self.data['input-param']
        output_folder = self.data['output-folder']
        ret = []
        for input_file, full_name in input_files:
            print(' - %s' % full_name)
            cmd = [exec_fullname, input_param, full_name]
            out_name = '%s_%s.txt' % (exec_name, input_file)
            out_file = path.join(output_folder, out_name)
            ret.append((cmd, input_file, out_file))
        return ret

    def commands(self):
        input_files = self.input_files()
        # For each executable type (t):
        for t, exec_name, exec_fullname in self.execs_fullname():
            print('\nRunning %s:.....' % exec_name)
            yield t, exec_name, \
                  self.exec_cmds(input_files, exec_name, exec_fullname)

    def specific_config(self, type_name):
        configs = self.data['configs']
        config_filter = (c for c in configs if type_name in c.get('types', []))
        config = next(config_filter, {})

        environments = parse_env(config.get('env', []))
        parameters = parse_param(config.get('param', []))
        return environments, parameters
