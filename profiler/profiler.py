import time
import traceback
import subprocess


def update_progress(i, n, size=100):
    print('\r   [ %-100s ] %d / %d' % ('#' * int(i / n * size), i, n), end='')


def perf(cmd, out_name, cwd, repeat, env):
    times = []
    with open(out_name, 'w') as fout:
        try:
            for i in range(0, repeat):
                update_progress(i, repeat)
                time.sleep(0.3)
                s_time = time.time()
                subprocess.check_call(cmd, cwd=cwd, stdout=fout, stderr=fout,
                                      env=env)
                times.append(time.time() - s_time)
            update_progress(repeat, repeat)
        except subprocess.CalledProcessError:
            traceback.print_exc()
        finally:
            print()

    return times
