#!/usr/bin/env python
import cli_parser
import modules

cli_params = cli_parser.read()
if cli_params.mode == 'perf':
    modules.eval_performance(cli_params.filename)
elif cli_params.mode == 'graph':
    modules.gen_graphs(cli_params.filename, cli_params.result)
